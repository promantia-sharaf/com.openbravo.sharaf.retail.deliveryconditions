/*
 ************************************************************************************
 * Copyright (C) 2018-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.deliveryconditions.processes;

import java.math.BigDecimal;

import javax.inject.Inject;

import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.plm.Product;

import com.openbravo.sharaf.retail.sapws.stock.StockSAPService;

public class CheckStockSAPService extends BaseCheckStock {

  @Inject
  private StockSAPService stockService;

  protected BigDecimal getStock(Product product, Warehouse warehouse, String stockLocation) {
    BigDecimal stock = stockService.getStock(product.getSearchKey(), warehouse.getSearchKey(),
        stockLocation);
    if (stock == null) {
      return BigDecimal.ZERO;
    }
    return stock;
  }
}